<?php
/**
 * Copyright (c) 2019. MagePro.dev
 * All rights reserved.
 */



\Magento\Framework\Component\ComponentRegistrar::register(
	\Magento\Framework\Component\ComponentRegistrar::LANGUAGE,
	'magepro_fr_fr',
	__DIR__
);